import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import Home from '../home/Home';
import NewPost from '../new-post/NewPost';
import Login from '../login/Login';

const Stack = createStackNavigator();

export default function Navigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator  headerMode={"none"}>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Home" component={Home}/>
                <Stack.Screen name="Segundo" component={NewPost}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}
