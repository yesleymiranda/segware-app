import React, {useEffect, useState} from 'react';
import {ContainerButtonNewPost, ContainerForm, HeaderForm} from './NewPostStyles';
import {NewPostImage} from '../../styles/ImagesStyle';
import {FormInputText} from '../../styles/FormsStyle';
import {Touch, TouchRadius} from '../../styles/TouchableStyle';
import {TextGreen14Center, TextTomato14, TextTomato14Center, TextWhite14} from '../../styles/TextStyle';
import {Flex1, ViewRowAroundItemCenter, ViewRowCenter} from '../../styles/ViewStyle';
import Loading from '../loading/Loading';
import {ContentErrorState} from '../login/LoginStyle';
import {postNewMessage} from '../../services/PostsService';
import {getUserStorage} from '../../storages/UserStorage';

const NewPost = props => {

    const [isSubmit, setIsSubmit] = useState(false);
    const [errorState, setErrorState] = useState(false);

    const [showForm, setShowForm] = useState(false);

    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');
    const [user, setUser] = useState(null);


    useEffect(() => {
        getUser();
    }, []);

    const handlerShowForm = () => {
        setShowForm(!showForm);
    };

    const getUser = async () => {
        await setUser(await getUserStorage());
    };

    const sendNewPost = async () => {
        try {
            if (!title) {
                throw ('Título obrigatório!');
            }
            if (!message || message.length < 5) {
                throw ('A mensagem da sua ideia é obrigatória e com tamanho mínimo de 10');
            }

            await postNewMessage(title, message, user);

            clearAndCloseForm();
        } catch (e) {
            errorMessage(e);
            setIsSubmit(false);
        }
    };

    const clearAndCloseForm = () => {
        setTitle('');
        setMessage('');
        setShowForm(false);
        props.callbackReflesh();
    };

    const errorMessage = message => {
        setErrorState(message);
        setTimeout((() => {
            setErrorState(null);
        }), 3000);
    };

    const renderStateError = () => {
        return (
            <ContentErrorState>
                <TextTomato14Center>{errorState || 'Algo deu errado, tente novamente!'}</TextTomato14Center>
            </ContentErrorState>
        );
    };

    return (
        <>
            <ContainerButtonNewPost>
                <Touch
                    onPress={handlerShowForm}
                    marginTop={'0px'}
                    style={{justifyContent: 'center', alignItems: 'center'}}>
                    <NewPostImage/>
                </Touch>
            </ContainerButtonNewPost>

            {showForm &&
            <ContainerForm>
                <HeaderForm>
                    <ViewRowAroundItemCenter style={{width: '100%'}}>
                        <TextWhite14>Nova ideia</TextWhite14>
                        <Touch h={'30px'} marginTop={'0px'} onPress={handlerShowForm}>
                            <TextTomato14>Fechar</TextTomato14>
                        </Touch>
                    </ViewRowAroundItemCenter>

                </HeaderForm>
                <Flex1 style={{width: '90%'}}>
                    <FormInputText
                        placeholder={'digite o titulo da postagem'}
                        value={title}
                        onChangeText={text => setTitle(text)}
                        maxLength={50}
                        autoCapitalize={'none'}
                        returnKeyType={"done"}
                    />

                    <FormInputText
                        style={{height: 150, textAlign: 'left'}}
                        placeholder={'deixe aqui a sua ideia'}
                        value={message}
                        onChangeText={text => setMessage(text)}
                        maxLength={1000}
                        multiline
                        numberOfLines={4}
                        returnKeyType={"done"}
                    />

                    <ViewRowCenter>
                        {isSubmit ?
                            <Loading/>
                            :
                            <TouchRadius onPress={sendNewPost}>
                                <TextGreen14Center>Enviar</TextGreen14Center>
                            </TouchRadius>
                        }
                    </ViewRowCenter>
                </Flex1>
            </ContainerForm>
            }
            {errorState && renderStateError()}
        </>
    );
};

export default NewPost;
