import styled from 'styled-components/native';
import {Green, White} from '../../styles/ColorsStyle';

export const ContainerButtonNewPost = styled.View`
  width: 60px;
  height: 60px;
  border-radius: 50px;
  background-color: ${Green};
  position: absolute;
  bottom: 50px;
  right: 20px;
  justify-content: center;
  align-items: center;
`;


export const ContainerForm = styled.View`
  width: 100%;
  height: 85%;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  background-color: ${White};
  position: absolute;
  bottom: -1px;
  align-self: center;
  align-items: center;
  padding-top: 30px;
  border-width: 1px;
  border-color: ${Green}
`;

export const HeaderForm = styled.View`
  width: 100%;
  height: 40px;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  background-color: ${Green};
  position: absolute;
  top: 0;
  justify-content: center;
  align-items: center;
`;
