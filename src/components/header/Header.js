import React, {useEffect, useState} from 'react';
import {Animated} from 'react-native';
import {HeaderContainer} from './HeaderStyle';
import {getUserStorage} from '../../storages/UserStorage';
import {TextWhite14Center} from '../../styles/TextStyle';
import {ViewRowCenterItemCenter} from '../../styles/ViewStyle';

const Header = props => {

    const [user, setUser] = useState(null);

    const scale = new Animated.Value(0);

    useEffect(() => {
        getUser();
        Animated.timing(scale, {toValue: 1}).start();
    }, []);

    const getUser = async () => {
        await setUser(await getUserStorage());
    };

    return (
        <HeaderContainer>
            <ViewRowCenterItemCenter style={{height: '100%'}}>
                <Animated.View style={{transform: [{scale: scale}]}}>
                    <TextWhite14Center>Olá, {user}</TextWhite14Center>
                </Animated.View>
            </ViewRowCenterItemCenter>
        </HeaderContainer>
    );
};

export default Header;
