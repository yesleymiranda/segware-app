import styled from 'styled-components/native';
import {Green} from '../../styles/ColorsStyle';

/**
 * FLEX
 **/

export const HeaderContainer = styled.View`
  width: 100%;
  height: 10%;
  border-bottom-left-radius: 15px ;
  border-bottom-right-radius: 15px ;
  background-color: ${Green};
`;
