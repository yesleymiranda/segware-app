import styled from 'styled-components/native';
import {GrayLight} from '../../styles/ColorsStyle';

export const ContainerAvatar = styled.View`
  width: 60px; 
  height: 60px;
  background-color: ${GrayLight};
  justify-content: center;
  align-items: center;  
`;
