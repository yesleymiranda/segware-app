import React, {useEffect, useState} from 'react';
import {Image, RefreshControl, Text} from 'react-native';
import {
    Card,
    ColumnAroundItemCenter,
    Divider,
    Flex1,
    ScrollViewCustom,
    ViewColumn,
    ViewColumnCenterItemCenter,
    ViewRow,
    ViewRowCenter,
} from '../../styles/ViewStyle';

import Header from '../header/Header';

import {getPosts, postUpVote} from '../../services/PostsService';
import {GrayLight, Green} from '../../styles/ColorsStyle';
import {ContainerAvatar} from './HomeStyle';
import {
    TextGrayLight12,
    TextGrayLight14Center,
    TextGreen12,
    TextGreen14,
    TextGreen14Center,
    TextTomato14,
    TextTomato14Center,
} from '../../styles/TextStyle';
import {Touch} from '../../styles/TouchableStyle';
import NewPost from '../new-post/NewPost';
import {UpVoteImage} from '../../styles/ImagesStyle';
import {ContentErrorState} from '../login/LoginStyle';

const Home = props => {

    const [loading, setLoading] = useState(true);
    const [errorState, setErrorState] = useState(false);
    const [errorRequestState, setErrorRequestState] = useState(false);

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        start();
    }, []);

    /** Busca as postagens
     * - Em caso de falha, habilita Error state
     */
    const start = async () => {
        try {
            setErrorState(false);
            setLoading(true);
            const response = await getPosts();
            setPosts(response);
            setLoading(false);
        } catch (e) {
            setLoading(false);
            setErrorRequestState(true);
        }
    };

    const handlerUpVote = async post => {
        try {
            setErrorState(false);
            await postUpVote(post);
            start();
        } catch (e) {
            errorMessage(e);
        }
    };

    const errorMessage = message => {
        setErrorState(message);
        setTimeout((() => {
            setErrorState(null);
        }), 3000);
    };

    const renderStateError = () => {
        return (
            <ContentErrorState>
                <TextTomato14Center>{errorState || 'Algo deu errado, tente novamente!'}</TextTomato14Center>
            </ContentErrorState>
        );
    };

    /** Reder para os cards com cada postagem **/
    const renderPostCards = (item, index) => {
        return (
            <ViewRowCenter key={index}>
                <Card>
                    <ViewRow>
                        <ViewColumn>
                            <ContainerAvatar>
                                <Image
                                    style={{width: 60, height: 60}}
                                    source={require('../../assets/images/avatars/boy.png')}/>
                            </ContainerAvatar>

                            <ViewRow style={{marginTop: 10}}>
                                <Touch
                                    onPress={() => handlerUpVote(item.id)}
                                    w={'30px'} h={'30px'} marginTop={'0px'}>
                                    <UpVoteImage/>
                                </Touch>
                                <ColumnAroundItemCenter>
                                    <TextGreen14>{item.points}</TextGreen14>
                                </ColumnAroundItemCenter>
                            </ViewRow>
                        </ViewColumn>

                        <ViewColumn style={{width: '80%', padding: 10}}>
                            <TextGreen12>@{item['user_name'].toLowerCase()}
                                <TextGrayLight12> - {item['created_at']}</TextGrayLight12></TextGreen12>
                            <Text>{item.title} </Text>
                            <Divider style={{marginVertical: 5}}/>
                            <Text>{item.description} </Text>

                        </ViewColumn>
                    </ViewRow>
                </Card>

            </ViewRowCenter>
        );
    };

    /** Error state, em caso de falha na consulta da API **/
    const renderErroState = () => {
        return (
            <ViewColumnCenterItemCenter style={{paddingTop: 20}}>
                <TextTomato14>Algo deu errado</TextTomato14>
                <TextGrayLight14Center>Por algum motivo, não conseguimos carregar as últimas
                    postagens</TextGrayLight14Center>
                <Touch w={'200px'} onPress={start}>
                    <TextGreen14Center>Tentar novamente</TextGreen14Center>
                </Touch>
            </ViewColumnCenterItemCenter>
        );
    };

    return (
        <Flex1 style={{backgroundColor: GrayLight}}>
            <Header/>
            <ScrollViewCustom
                refreshControl={
                    <RefreshControl
                        refreshing={loading}
                        onRefresh={start}
                        colors={[Green]}/>
                }>

                <Flex1 style={{paddingBottom: 150}}>
                    {posts && posts.map((item, index) => renderPostCards(item, index))}
                    {errorRequestState && renderErroState()}
                </Flex1>
            </ScrollViewCustom>

            <NewPost callbackReflesh={start}/>

            {errorState && renderStateError()}
        </Flex1>
    );
};

export default Home;
