import React from 'react';
import {ActivityIndicator} from 'react-native';
import {ViewColumnCenterItemCenter} from '../../styles/ViewStyle';
import {Green} from '../../styles/ColorsStyle';

const Loading = (props) => {

    return (
        <ViewColumnCenterItemCenter style={{width: '100%', marginTop: 20}}>
            <ActivityIndicator size="large" color={props.color ? props.color : Green}/>
        </ViewColumnCenterItemCenter>
    );

};

export default Loading;
