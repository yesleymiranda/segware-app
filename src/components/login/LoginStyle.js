import styled from 'styled-components/native';
import {Tomato, White} from '../../styles/ColorsStyle';

export const ContentErrorState = styled.View`
  width: 80%;
  height: 50px;
  background-color: ${White};
  border-radius: 14px;
  position: absolute;
  bottom: 40px;
  align-self: center;
  justify-content: center;
  align-items: center;
  border-width: 1px;
  border-color: ${Tomato};
`;

export const ContentLoginTop = styled.View`
  background-color: burlywood;

`;

