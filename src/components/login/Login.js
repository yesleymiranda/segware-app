import React, {useState} from 'react';
import {
    Divider,
    Flex1Green,
    Flex3White,
    FlexColumnAroundItemCenter,
    ViewColumnCenterItemCenter,
} from '../../styles/ViewStyle';
import {IdeiasImage} from '../../styles/ImagesStyle';
import {FormInputText} from '../../styles/FormsStyle';
import {TextGrayLight14Center, TextGreen14Center, TextTomato14Center} from '../../styles/TextStyle';
import {TouchRadius} from '../../styles/TouchableStyle';
import {setUserStorage} from '../../storages/UserStorage';
import {ContentErrorState} from './LoginStyle';
import Loading from '../loading/Loading';


const Login = props => {

    const [errorState, setErrorState] = useState(false);
    const [isSubmit, setIsSubmit] = useState(false);
    const [userName, setUserName] = useState('');

    /** Uma autenticação fake, só para conseguir um nome de usuário e vincular as postagens **/
    const fakeAuthentication = async () => {
        try {

            setErrorState(false);
            setIsSubmit(true);

            if (!userName) {
                throw ('Tag obrigatório!');
            }
            if (userName.length < 5) {
                throw ('Tag tamanho mínimo 5!');
            }

            await setUserStorage(userName);
            props.navigation.navigate('Home');

        } catch (e) {
            errorMessage(e);
            setIsSubmit(false);
        }
    };

    const errorMessage = message => {
        setErrorState(message);
        setTimeout((() => {
            setErrorState(null);
        }), 3000);
    };

    const renderStateError = () => {
        return (
            <ContentErrorState>
                <TextTomato14Center>{errorState || 'Algo deu errado, tente novamente!'}</TextTomato14Center>
            </ContentErrorState>
        );
    };

    return (
        <Flex1Green>

            <Flex1Green>
                <FlexColumnAroundItemCenter>
                    <IdeiasImage/>
                </FlexColumnAroundItemCenter>
            </Flex1Green>
            <Flex3White>
                <FlexColumnAroundItemCenter>

                    <ViewColumnCenterItemCenter style={{width: '80%', height: 260}}>

                        <ViewColumnCenterItemCenter style={{width: '100%'}}>

                            <FormInputText
                                placeholder={'digite sua tag fake'}
                                value={userName}
                                onChangeText={text => setUserName(text.replace(/ /g, ''))}
                                maxLength={30}
                                autoCapitalize={'none'}
                            />
                            {isSubmit ?
                                <Loading/>
                                :
                                <TouchRadius onPress={fakeAuthentication}>
                                    <TextGreen14Center>Entrar</TextGreen14Center>
                                </TouchRadius>
                            }

                            <TextGrayLight14Center>Esqueceu a senha ?</TextGrayLight14Center>

                        </ViewColumnCenterItemCenter>

                    </ViewColumnCenterItemCenter>

                    <Divider/>
                    <TextGrayLight14Center>© 2020 IDEIAS v.1.0.0</TextGrayLight14Center>
                    <Divider/>

                </FlexColumnAroundItemCenter>
            </Flex3White>


            {errorState && renderStateError()}
        </Flex1Green>
    );
};

export default Login;
