import ApiSegware from './ApiSegware';

export const getPosts = async () => {

    try {
        const request = await ApiSegware.get(`/posts`,
            {
                timeout: 10000,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                validateStatus: function (status) {
                    return status >= 200 && status <= 401;
                },
            },
        );

        if (request.status === 200) {
            return request.data.response;
        } else {
            throw 'Não foi possivel buscas postagens!';
        }
    } catch (e) {
        throw 'Não foi possivel buscas postagens!';
    }
};

export const postNewMessage = async (title, message, user_name) => {

    try {
        const request = await ApiSegware.post(`/posts`,
            {
                titulo: title,
                descricao: message,
                nome_usuario: user_name,
            },
            {
                timeout: 10000,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                validateStatus: function (status) {
                    return status >= 200 && status <= 401;
                },
            },
        );

        if (request.status === 201) {
            return true;
        } else {
            throw 'Não foi possível enviar uma nova ideia!';
        }
    } catch (e) {
        throw 'Não foi possível enviar uma nova ideia!';
    }
};

export const postUpVote = async (post_id) => {
    try {
        const request = await ApiSegware.put(`/posts/points`,
            {
                postagem_id: post_id,
                acao: 'upvote',
            },
            {
                timeout: 10000,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                validateStatus: function (status) {
                    return status >= 200 && status <= 401;
                },
            },
        );

        if (request.status === 200) {
            return true;
        } else {
            throw 'Não foi possível votar na ideia!';
        }
    } catch (e) {
        throw 'Não foi possível votar na ideia!';
    }
};
