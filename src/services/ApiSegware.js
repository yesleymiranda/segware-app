import axios from 'axios';

// TODO Fixo somente para APP de exmplo, o melhor é incluir dinamica, como exemplo dotenv verificando modo dev/prod/...
const ApiSegware = axios.create({
    baseURL: 'http://192.168.0.15:9052/api'
});

export default ApiSegware;
