import AsyncStorage from '@react-native-community/async-storage';

const USER_STORAGE_KEY = '@ideias-user';

export const setUserStorage = async user => {
    try {
        await AsyncStorage.setItem(USER_STORAGE_KEY, user);
    } catch (e) {
        throw e;
    }
};

export const getUserStorage = async () => {
    try {
        return await AsyncStorage.getItem(USER_STORAGE_KEY);
    } catch (e) {
        throw e;
    }
};

