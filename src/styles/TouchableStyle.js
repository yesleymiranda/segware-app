import styled from 'styled-components/native';
import {Green} from './ColorsStyle';

export const Touch = styled.TouchableOpacity.attrs({

})`
  width: ${props => props.w || '50px'};
  height: ${props => props.h || '50px'};
  align-items: ${props => props.alignItems || 'center'};
  margin-top: ${props => props.marginTop || '20px'};
  justify-content: ${props => props.justifyContent || 'center'};
`;

export const TouchRadius = styled.TouchableOpacity.attrs({

})`
  width: ${props => props.w || '200px'};
  height: ${props => props.h || '50px'};
  align-items: ${props => props.alignItems || 'center'};
  margin-top: ${props => props.marginTop || '20px'};
  margin-bottom: 20px;
  justify-content: ${props => props.justifyContent || 'center'};
  border-width: 1px;
  border-color: ${Green};
  border-radius: 15px;
`;
