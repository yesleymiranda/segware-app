import styled from 'styled-components/native';
import {Gray, Green, Tomato, White} from './ColorsStyle';

/**
 * TEXT's
 */
export const TextGreen = styled.Text`
  color: ${Green};
`;

export const TextGrayLight = styled.Text`
  color: ${Gray};
`;

export const TextTomato = styled.Text`
  color: ${Tomato};
`;

export const TextWhite = styled.Text`
  color: ${White};
`;

/**
 * TEXT's 12
 */
export const TextGreen12 = styled(TextGreen)`
  font-size: 12px;
`;

export const TextGrayLight12 = styled(TextGrayLight)`
  font-size: 12px;
`;

export const TextTomato12 = styled(TextTomato)`
  font-size: 12px;
`;

export const TextWhite12 = styled(TextWhite)`
  font-size: 12px;
`;

/**
 * TEXT's 14
 */
export const TextGreen14 = styled(TextGreen)`
  font-size: 14px;
`;

export const TextGrayLight14 = styled(TextGrayLight)`
  font-size: 14px;
`;

export const TextTomato14 = styled(TextTomato)`
  font-size: 14px;
`;

export const TextWhite14 = styled(TextWhite)`
  font-size: 14px;
`;

/**
 * TEXT's 14 CENTER
 */
export const TextGreen14Center = styled(TextGreen14)`
  text-align: center;
`;

export const TextGrayLight14Center = styled(TextGrayLight14)`
  text-align: center;
`;

export const TextTomato14Center = styled(TextTomato14)`
  text-align: center;
`;

export const TextWhite14Center = styled(TextWhite)`
  text-align: center;
`;
