import styled from 'styled-components/native';
import {Gray, GrayLight} from './ColorsStyle';

export const FormInputText = styled.TextInput`
  width: 100%;
  padding-top: ${Platform.select({ios: 5, android: 0})}px;
  padding-bottom: ${Platform.select({ios: 2, android: 0})}px;
  background-color: ${GrayLight};
  border-radius: 10px;
  height: 40px;
  margin-top: 30px;
  padding-left: 10px;
  text-align: center;
`;
