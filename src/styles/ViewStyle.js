import styled from 'styled-components/native';
import {ScrollView, StyleSheet} from 'react-native';
import {GrayLight, Green, White} from './ColorsStyle';

/**
 * FLEX
 **/

export const Flex1 = styled.View`
  flex: 1;
`;

export const Flex2 = styled.View`
  flex: 2;
`;

export const Flex3 = styled.View`
  flex: 3;
`;

/**
 * FLEX White
 **/

export const Flex1White = styled(Flex1)`
  background-color: ${White};
`;

export const Flex2White = styled(Flex2)`
  background-color: ${White};
`;

export const Flex3White = styled(Flex3)`
  background-color: ${White};
`;


/**
 * FLEX Green
 **/

export const Flex1Green = styled(Flex1)`
  background-color: ${Green};
`;

export const Flex2Green = styled(Flex2)`
  background-color: ${Green};
`;

export const Flex3Green = styled(Flex3)`
  background-color: ${Green};
`;


/**
 * VIEW Rows
 **/

export const ViewRow = styled.View`
    flex-direction: row;
`;

export const ViewRowCenter = styled(ViewRow)`
    justify-content: center;
`;

export const ViewRowAround = styled(ViewRow)`
    justify-content: space-around;
`;

export const ViewRowBetween = styled(ViewRow)`
    justify-content: space-between;
`;

export const ViewRowCenterItemCenter = styled(ViewRowCenter)`
    align-items: center;
`;


export const ViewRowAroundItemCenter = styled(ViewRowAround)`
    align-items: center;
`;


/**
 * VIEW Columns
 **/
export const ViewColumn = styled.View`
    flex-direction: column;
`;

export const ViewColumnCenterItemCenter = styled(ViewColumn)`
    justify-content: center;
    align-items: center;    
`;

export const ColumnAroundItemCenter = styled.View`
    flex:1;
    flex-direction: column;    
    justify-content: space-around;
    align-items: center;
`;

/**
 * FLEX Columns
 **/
export const FlexColumnCenterItemCenter = styled.View`
    flex:1;
    flex-direction: column;
    align-items: center;
`;

export const FlexColumnBetweenItemCenter = styled.View`
    flex:1;
    flex-direction: column;    
    justify-content: space-between;
    align-items: center;
`;

export const FlexColumnAroundItemCenter = styled.View`
    flex:1;
    flex-direction: column;    
    justify-content: space-around;
    align-items: center;
`;


/**
 * CARDS
 **/
export const Card = styled.View`
    width: 94%;
    min-height: 80px;
    border-color: ${White};
    border-width: ${StyleSheet.hairlineWidth}px;
    border-radius: 5px;
    margin: 10px 5px 5px 5px;
    elevation: 2;
    background-color: white;
    shadowOffset: 0px 3px;
    shadowOpacity: 0.23;
    shadowRadius: 5px;
    padding: 5px;
`;


/**
 * DIVIDER
 **/
export const Divider = styled.View`
  width: 100%;
  border-top-width: ${StyleSheet.hairlineWidth}px;
  border-color:  ${GrayLight};
`;


/**
 * SCROLL VIEW's
 **/
export const ScrollViewCustom = styled(ScrollView).attrs({
    contentContainerStyle: {
        paddingHorizontal: 5,
    },
})`
`;
