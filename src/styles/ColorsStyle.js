
/**
 * Colors
 */
export const White = `#FFFFFF`;
export const GrayLight = `#F5F5F7`;
export const Gray = `#6d6d6d`;
export const Green = `#01B58E`;
export const Tomato = `#ff6347`;
