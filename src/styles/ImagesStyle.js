import styled from 'styled-components/native';
import {Image} from 'react-native';

export const IdeiasImage = styled(Image).attrs({
    source: require('../assets/images/ideias.png'),
})`
  border-radius: 15px;
  position: absolute;
  bottom: -40px;
`;

export const NewPostImage = styled(Image).attrs({
    source: require('../assets/images/new-post.png'),
})`
  width: 40px;
  height: 40px;
`;

export const UpVoteImage = styled(Image).attrs({
    source: require('../assets/images/upvote.png'),
})`
  width: 25px;
  height: 25px;
`;
