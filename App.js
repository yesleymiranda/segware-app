import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import Navigation from './src/components/navigation/Navigation';

import {Flex1} from './src/styles/ViewStyle';

const App = () => {
    return (
        <>
            <StatusBar barStyle="dark-content"/>
            <SafeAreaView style={{flex: 1}}>
                <Flex1 style={{backgroundColor: 'red'}}>
                    <Navigation/>
                </Flex1>
            </SafeAreaView>
        </>
    );
};

export default App;
